<?php

return [
    "1" => "Pure and Applied Mathematics",
    "10" => "Water",
    "11" => "Biotechnology",
    "2" => "Computational Science",
    "24" => "Host-Microbe interactions in Health, Water &amp; Agriculture",
    "25" => "Habitability of the Earth &amp; Planets",
    "26" => "Marine Biology and Ecology",
    "27" => "Chemical &amp; Synthetic Biology",
    "28" => "AI &amp; Big Data",
    "29" => "Science for sustainability",
];