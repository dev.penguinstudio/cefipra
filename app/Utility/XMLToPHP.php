<?php

namespace App\Utility;

class XMLToPHP {

    private $xml_response;

    public function __construct($response) {
        $this->xml_response = $response;
    }

    public function getPHPResponse() {
        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $this->xml_response);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $responseArray = json_decode($json,true);
        $final_response = array();
        if(count($responseArray) > 0) {
            if((array_key_exists('unit', $responseArray['UOM']))) {
                $reponse_sub_area = $responseArray['UOM']['unit'];
                foreach($reponse_sub_area as $sub_area) {
                    if(array_key_exists('@attributes', $sub_area)) {
                        $name_key = $sub_area['@attributes']['name'];
                        $code_value = $sub_area['@attributes']['code'];
                        $final_response += [$name_key => $code_value];
                    } else {
                        $name_key = $sub_area['name'];
                        $code_value = $sub_area['code'];
                        $final_response += [$name_key => $code_value];
                    }
                }
            }
        }
        return $final_response;
    }
}