<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //

    public function index() {
        $all_applicants = count(User::where([
            'status' => '1',
            'role' => '3'
            ])->get());
        return view('admin.dashboard')->with([
            'no_of_applicants' => $all_applicants
        ]);
    }
}
