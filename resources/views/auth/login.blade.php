@extends('layouts.admin.app-auth', ['page_title' => 'Login'])

@section('content')
<div class="card">
    <div class="card-body login-card-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <form action="{{ route('login') }}" method="post">
            @csrf
            <div class="input-group mb-3">
                <input type="email" class="form-control" placeholder="Email" name="email">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="far fa-envelope"></span>
                    </div>
                </div>
            </div>
            <div class="input-group mb-3">
                <input type="password" class="form-control" placeholder="Password" name="password">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-8">
                    <div class="icheck-primary">
                        <input type="checkbox" id="remember" name="remember">
                        <label for="remember">
                            Remember Me
                        </label>
                    </div>
                </div>
                <div class="col-4">
                    <button type="submit" class="btn btn-outline-primary btn-block">Sign In</button>
                </div>
            </div>
        </form>

        <p class="mb-1">
            <a href="{{ route('password.request') }}">I forgot my password</a>
        </p>

        <p class="mb-0">
            <a href="{{ route('register') }}" class="text-center">New Here? Register Now</a>
        </p>
    </div>
    <!-- /.login-card-body -->
</div>
@endsection
