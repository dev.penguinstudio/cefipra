<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>{{ config('custom.app_name') }}</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <link href="{{ asset('../resources/img/c_logo.png') }}" rel="icon">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;500;600&display=swap" rel="stylesheet">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <link href="{{ asset('../resources/lib/animate/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('../resources/lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">

    <link href="{{ asset('../resources/css/web/bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ asset('../resources/css/web/style.css') }}" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/jquery"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
</head>

<body>
    <!-- Spinner Start -->
    <div id="spinner"
        class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
        <div class="spinner-border text-primary" role="status" style="width: 3rem; height: 3rem;"></div>
    </div>
    <!-- Spinner End -->


    <!-- Topbar Start -->
    <div class="container-fluid bg-dark px-0">
        <div class="row g-0 d-none d-lg-flex">
            <div class="col-lg-6 ps-5 text-start">
                <div class="h-100 d-inline-flex align-items-center text-white">
                    <span>Follow Us:</span>
                    <a class="btn btn-link text-light" target="_blank" href="https://www.facebook.com/CEFIPRA/"><i class="fab fa-facebook-f"></i></a>
                    <a class="btn btn-link text-light" target="_blank"  href="https://twitter.com/IFCPAR"><i class="fab fa-twitter"></i></a>
                    <a class="btn btn-link text-light" target="_blank"  href="https://www.youtube.com/@cefipra.ifcpar/videos"><i class="fab fa-youtube"></i></a>
                    <a class="btn btn-link text-light" target="_blank"  href="https://web.whatsapp.com/send?phone= {{ config('custom.whatsapp_number') }}"><i class="fab fa-whatsapp"></i></a>
                </div>
            </div>
            <div class="col-lg-6 text-end">
                <div class="h-100 topbar-right d-inline-flex align-items-center text-white py-2 px-5">
                    <span class="fs-5 fw-bold me-2"><i class="far fa-envelope me-2"></i>Mail Us:</span>
                    <span class="fs-5 fw-bold">
                        <a class="text-white" href="mailto:contact.ifcpar@cefipra.org">contact.ifcpar@cefipra.org</a>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <!-- Topbar End -->


    <!-- Navbar Start -->
    <nav class="navbar navbar-expand-lg bg-white navbar-light sticky-top py-0 pe-5">
        <a href="index.html" class="navbar-brand ps-5 me-0">
            <h1 class="text-white m-0">{{ config('custom.app_name_fr') }}</h1>
        </a>
        <button type="button" class="navbar-toggler me-0" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <div class="navbar-nav ms-auto p-4 p-lg-0">
                <a href="{{ route('web-home') }}" class="nav-item nav-link {{ Route::is('*home') ? 'active' : '' }}">Home</a>
                <a href="" class="nav-item nav-link">About</a>
                <a href="" class="nav-item nav-link">Projects</a>
                <a href="" class="nav-item nav-link">Activities</a>
                <a href="{{ route('contact-us') }}" class="nav-item nav-link {{ Route::is('contact-us') ? 'active' : '' }}">Contact Us</a>
                @if (auth()->id())
                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                        Hello {{ auth()->user()->salutation }} {{ auth()->user()->first_name }}
                    </a>
                    <div class="dropdown-menu bg-light m-0">
                        <a href="" class="dropdown-item">
                            <i class="far fa-user"></i>
                            My Profile
                        </a>
                        <a href="" class="dropdown-item">
                            <i class="far fa-newspaper"></i>
                            My Research
                        </a>
                        <a href="#" onclick="logOutButtonAction()" class="dropdown-item">
                            <i class="fa fa-sign-out-alt"></i>
                            Logout
                        </a>
                    </div>
                </div>
                @endif
            </div>
            @if (auth()->id())
            @else
            <a href="{{ route('login') }}" class="btn btn-primary px-3 d-none d-lg-block">Login / Register</a>
            @endif
        </div>
    </nav>
    <!-- Navbar End -->

    @yield('contents')
    
    <!-- Footer Start -->
    <div class="container-fluid bg-dark footer mt-5 py-5 wow fadeIn" data-wow-delay="0.1s">
        <div class="container py-5">
            <div class="row g-5">
                <div class="col-lg-3 col-md-6">
                    <h5 class="text-white mb-4">Quick Links</h5>
                    <a class="btn btn-link" href="">Annual Report</a>
                    <a class="btn btn-link" href="">Brochure</a>
                    <a class="btn btn-link" href="">CEFIPRA Publication</a>
                    <a class="btn btn-link" href="">Director Profile</a>
                    <a class="btn btn-link" href="">Links for Reference</a>
                </div>

                <div class="col-lg-3 col-md-6">
                    <h5 class="text-white mb-4">Members</h5>
                    <a class="btn btn-link" href="">Governing Body (GB)</a>
                    <a class="btn btn-link" href="">Scientific Council (SC)</a>
                    <a class="btn btn-link" href=""><small>Industrial Research Committee (IRC)</small></a>
                    <a class="btn btn-link" href=""><small>Standard Expert Panel (SEP)</small></a>
                </div>
                
                <div class="col-lg-3 col-md-6">
                    <h5 class="text-white mb-4">Policies/Guidelines</h5>
                    <a class="btn btn-link" href="">Conflict of Interest</a>
                    <a class="btn btn-link" href="">IP Guidelines</a>
                </div>
                
                <div class="col-lg-3 col-md-6">
                    <h5 class="text-white mb-4">Working Hours</h5>
                    <p class="mb-1">Monday - Friday</p>
                    <h6 class="text-light">09:30 am - 06:00 pm</h6>
                    <p class="mb-1">Saturday &amp; Sunday</p>
                    <h6 class="text-light">Closed</h6>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer End -->

    <!-- Copyright Start -->
    <div class="container-fluid copyright bg-dark py-4">
        <div class="container text-center">
            <p>All Right Reserved.</p>
        </div>
    </div>
    <form action="{{ route('logout') }}" method="POST" id="logout-form">
        @csrf
    </form>
    <!-- Copyright End -->


    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square rounded-circle back-to-top"><i
            class="bi bi-arrow-up"></i></a>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('../resources/lib/wow/wow.min.js') }}"></script>
    <script src="{{ asset('../resources/lib/easing/easing.min.js') }}"></script>
    <script src="{{ asset('../resources/lib/waypoints/waypoints.min.js') }}"></script>
    <script src="{{ asset('../resources/lib/owlcarousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('../resources/lib/counterup/counterup.min.js') }}"></script>
    <script src="{{ asset('../resources/js/web/main.js') }}"></script>

    <script>
        $(document).ready( function () {
            showMessages();
        });
        function showMessages() {
            var allErrors = ('{{ $errors->first() }}');
            if (allErrors.includes('phone')) {
                allErrors = allErrors.replace('phone', 'mobile number');
            }
            if (allErrors) {
                Swal.fire({
                    icon: 'error',
                    title: 'Sorry!',
                    text: allErrors
                })
            }
            
            var allSuccess = ('{{ session("success") }}');
            console.log(allSuccess);
            if (allSuccess) {
                Swal.fire({
                    icon: 'success',
                    title: 'Success',
                    text: allSuccess
                })
            }
        }

        function logOutButtonAction() {
            Swal.fire({
                title: 'Alert!',
                text: 'Are you sure you want to logout?',
                icon: 'question',
                showCancelButton: true,
                cancelButtonText: 'No',
                confirmButtonText: 'Yes',
                confirmButtonColor: '#d33724',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    document.getElementById('logout-form').submit();
                }
            });
        }
    </script>
</body>
</html>