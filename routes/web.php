<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\ResearcherController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('web.home');
})->name('web-home');

Route::get('contactUs', function () {
    return view('web.contact-us');
})->name('contact-us');

Route::get('changeLanguage', [LanguageController::class, 'languageChange'])->name('language');

Route::get('admin', function () {
    return view('admin.master');
});

Route::get('web', function () {
    return view('web.master');
});
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('country', function() {
    $contents = file_get_contents('https://gist.githubusercontent.com/anubhavshrimal/75f6183458db8c453306f93521e93d37/raw/f77e7598a8503f1f70528ae1cbf9f66755698a16/CountryCodes.json');
    $php_array = json_decode($contents);
    $final_response = [];
    foreach($php_array as $country) {
        $final_response += [
            $country->code => [
                'name' => $country->name,
                'dial_code' => $country->dial_code,
                'code' => $country->code
            ]
        ];
    }
    return $php_array;
});
Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('allApplicants', [ResearcherController::class, 'index'])->name('view.researchers');

