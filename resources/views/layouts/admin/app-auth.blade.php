<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('custom.app_name_fr') }} &mdash; {{ $page_title ?? "" }}</title>

    <link href="{{ asset('../resources/img/c_logo.png') }}" rel="icon">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('../resources/plugins/admin/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('../resources/css/admin/style.css') }}">
    <script src="https://cdn.jsdelivr.net/npm/jquery"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
</head>

<body class="hold-transition login-page gradient-default">
    @if (Route::is('register'))
    <div class="register-box">
    @else
    <div class="login-box">
    @endif
    
        <div class="login-logo ">
            <a href="" class="text-white">
                <img src="{{ asset('../resources/img/c_logo.png') }}" alt="" style="width: 50px;">
                <b>{{ config('custom.app_name_fr') }}</b>
            </a>
        </div>
        @yield('content')
    </div>

    <script src="{{ asset('../resources/plugins/admin/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('../resources/js/admin/main.js') }}"></script>

    <script>
                $(document).ready(function() {
			catchMessage()
		});

		function catchMessage() {
			var allErrors = ('{{ $errors->first() }}');
            if (allErrors) {
				showMessage("error", allErrors)
            }
            var allSuccess = ('{{ session("success") }}');
            if (allSuccess) {
				showMessage("success", allSuccess)
            }
		}

		function showMessage(type="success", context) {
			toastr.options = {
  				"closeButton": false,
  				"debug": false,
  				"newestOnTop": true,
  				"progressBar": false,
  				"positionClass": "toast-top-right",
  				"preventDuplicates": true,
  				"onclick": null,
  				"showDuration": "300",
  				"hideDuration": "1000",
  				"timeOut": "5000",
  				"extendedTimeOut": "1000",
  				"showEasing": "swing",
  				"hideEasing": "linear",
  				"showMethod": "fadeIn",
  				"hideMethod": "fadeOut"
			}
			switch (type) {
				case "error":
					toastr.error(context)
					break;
				case "warning":
					toastr.warning(context)
					break;
				case "success":
					toastr.success(context)
			}
		}
    </script>
</body>

</html>