@extends('layouts.admin.app-auth', ['page_title' => 'Register'])

@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<div class="card">
    <div class="card-body register-card-body">
        <p class="login-box-msg">Register your membership</p>
        <p class="text-danger">
            - Scientists: Scientists from India and France have to register separately for the submission of proposal. <br>
            - Council Member: No need to register. Please contact CEFIPRA administration for login details.
        </p>
        <form action="{{ route('register') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-2">
                    <div class="input-group mb-3">
                        <select name="salutation" id="" class="form-control">
                            <option value="">-- Salutation --</option>
                            <option value="Mr.">Mr.</option>
                            <option value="Dr.">Dr.</option>
                            <option value="Prof.">Prof.</option>
                            <option value="Ms.">Ms.</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="First Name" name="first_name">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="far fa-user"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Middle Name" name="middle_name">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="far fa-user"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Last Name" name="last_name">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="far fa-user"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="input-group mb-3">
                        <input type="email" class="form-control" placeholder="Email" name="email">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="far fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" placeholder="Password" name="password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fa fa-lock"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-3">
                    <div class="input-group mb-3">
                        <input type="date" class="form-control" placeholder="Date Of Birth" name="date_of_birth" max="{{ date('Y-m-d') }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fa fa-calendar-alt"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <select name="gender" id="" class="form-control">
                        <option value="">-- Select Gender --</option>
                        <option value="1">Male</option>
                        <option value="2">Female</option>
                        <option value="3">Other</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <select name="country" id="" class="form-control select2">
                        <option value="">-- Select Country --</option>
                        @foreach ($countries as $country)
                            <option value="{{ $country->code }}">{{ $country->name }} ({{ $country->dial_code }})</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3">
                    <div class="input-group mb-3">
                        <input type="tel" class="form-control" placeholder="Mobile Number" name="mobile_number">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fa fa-mobile-alt"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <hr>

            <div class="row">
                <div class="col-md-6">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="ORCID number / Researchers Id" name="research_id">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fa fa-list"></span>
                            </div>
                        </div>
                        <label class="text-danger">
                            NOTE: ORCID number/Researchers Id is mandatory for the PIs having affiliation from Academic/Research Institutions. For PIs affiliated to industrial organizations, they can fill "NA"
                        </label>
                    </div>
                </div>
                <div class="col-md-6">
                    <select name="" id="" class="form-control">
                        <option value="">-- Purpose of association with CEFIPRA --</option>
                        <option value="1">Principal Collaborator</option>
                        <option value="2">Referee</option>
                        <option value="3">Principal Collaborator & Referee</option>
                        <option value="4">Student</option>
                        <option value="5">Others</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <select name="qualification" id="" class="form-control">
                        <option value="">-- Highest Qualification --</option>
                        <option value="master">Masters</option>
                        <option value="phd">PhD</option>
                        <option value="other">Other</option>
                    </select>
                </div>
                <div class="col-md-3" hidden>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Qualification" name="other_qualification">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fa fa-graduation-cap"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <select name="major_area" id="major-area" class="form-control" onchange="getSubAreaOfResearch()">
                        <option value="">Major Area of Research</option>
                        @foreach (config('misc.areaOfResearch') as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-6">
                    <select name="sub_area_of_interest[]" id="sub-area-of-interest" class="form-control select2" multiple data-placeholder="Sub-Area(s) of Research" style="width: 100%">
                    </select>
                </div>
            </div>

            <hr>

            
            <div class="row">
                <div class="col-md-3">
                    <select name="work_country" id="work-country" class="form-control" onchange="getAllStates()">
                        <option value="">-- Work Country --</option>
                        <option value="170">France</option>
                        <option value="193">India</option>
                    </select>
                </div>

                <div class="col-md-3">
                    <select name="state" id="state" class="form-control" onchange="getAllCities()">
                        <option value="">-- State/Region --</option>
                    </select>
                </div>

                <div class="col-md-3">
                    <select name="district" id="district" class="form-control">
                        <option value="">-- District/City --</option>
                    </select>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputFile">Upload your photo</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input"
                                    id="photograph" name="photograph" accept="image/*" onchange="checkFileBeforeLoad()">
                                <label class="custom-file-label"
                                    for="exampleInputFile">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputFile">Upload your Signature</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input"
                                    id="signature" name="signature" accept="image/*" onchange="checkSignBeforeLoad()">
                                <label class="custom-file-label"
                                    for="exampleInputFile">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <img id="photograph-imageFile" style="object-fit: cover; max-width: 100%" src="{{ $banner_data->image_path ?? "" }}" >
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <img id="signature-imageFile" style="object-fit: cover; max-width: 100%" src="{{ $banner_data->image_path ?? "" }}">
                    </div>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-12 text-center">
                    <button type="submit" class="btn btn-outline-primary">Register</button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.login-card-body -->
</div>

<script>
    $(document).ready(function() {
        $('.select2').select2({
            theme: "classic"
        })
    });

    async function getSubAreaOfResearch() {
        let researchId = $('#major-area').find(":selected").val();
        await fetch('http://localhost/cefipra/cefipra/public/api/internal/subArea/' + researchId)
        .then(response => response.json())
        .then(data => {
            $('#sub-area-of-interest').empty().trigger('change');

            for (let [key, value] of Object.entries(data)) {                
                $('#sub-area-of-interest').append(`<option value="${key}">${value}</option>`);
            }
            $("#sub-area-of-interest").trigger('change');
        });
    }


    async function getAllStates() {
        let workCountry = $('#work-country').find(":selected").val();
        await fetch('http://localhost/cefipra/cefipra/public/api/internal/getStates/' + workCountry)
        .then(response => response.json())
        .then(data => {
            $('#state').empty();
            
            $('#state').append(`<option value="">-- State/Region --</option>`);
            for (let [key, value] of Object.entries(data)) {                
                $('#state').append(`<option value="${value}">${key}</option>`);
            }
        });
    }

    async function getAllCities() {
        let state = $('#state').find(":selected").val();
        await fetch('http://localhost/cefipra/cefipra/public/api/internal/getCities/' + state)
        .then(response => response.json())
        .then(data => {
            $('#district').empty();
            
            $('#district').append(`<option value="">-- District/City --</option>`);
            for (let [key, value] of Object.entries(data)) {                
                $('#district').append(`<option value="${value}">${key}</option>`);
            }
        });
    }

    function checkFileBeforeLoad() {
        let file = document.getElementById('photograph').files[0];
        var img;
        // if(file.size > 204800) {
        //     Swal.fire({
        //         icon: 'warning',
        //         title: 'Alert!',
        //         text: "File size is exceeded maximum limit (2MB)"
        //     });
        //     return;
        // }
        if(file.type.match('image.*') == false) {
            Swal.fire({
                icon: 'warning',
                title: 'Alert!',
                text: "Please upload an image file."
            });
            return;
        }

        loadFileDetails();
    }
    
    function loadFileDetails() {
        sizeOf = function (bytes) {
            if (bytes == 0) { return "0.00 B"; }
            var e = Math.floor(Math.log(bytes) / Math.log(1024));
            return (bytes/Math.pow(1024, e)).toFixed(2)+' '+' KMGTP'.charAt(e)+'B';
        }
        let file = document.getElementById('photograph').files[0];
        // let fileName = `<span>File Name: ${file.name}</span><br>`;
        // let fileSize = `<span>File Size: ${sizeOf(file.size)}</span><br>`;
        // let fileType = `<span>File Type: ${file.type}</span><br>`;
        // document.getElementById('photograph-file-details').innerHTML = fileName + fileSize + fileType;

        var reader = new FileReader();
        var imgtag = document.getElementById('photograph-imageFile');
        reader.onload = function(event) {
            imgtag.src = event.target.result;
        };
        reader.readAsDataURL(file);
    }

    function checkSignBeforeLoad() {
        let file = document.getElementById('signature').files[0];
        var img;
        if(file.type.match('image.*') == false) {
            Swal.fire({
                icon: 'warning',
                title: 'Alert!',
                text: "Please upload an image file."
            });
            return;
        }

        loadSignFileDetails();
    }

    function loadSignFileDetails() {
        sizeOf = function (bytes) {
            if (bytes == 0) { return "0.00 B"; }
            var e = Math.floor(Math.log(bytes) / Math.log(1024));
            return (bytes/Math.pow(1024, e)).toFixed(2)+' '+' KMGTP'.charAt(e)+'B';
        }
        let file = document.getElementById('signature').files[0];
        // let fileName = `<span>File Name: ${file.name}</span><br>`;
        // let fileSize = `<span>File Size: ${sizeOf(file.size)}</span><br>`;
        // let fileType = `<span>File Type: ${file.type}</span><br>`;
        // document.getElementById('photograph-file-details').innerHTML = fileName + fileSize + fileType;

        var reader = new FileReader();
        var imgtag = document.getElementById('signature-imageFile');
        reader.onload = function(event) {
            imgtag.src = event.target.result;
        };
        reader.readAsDataURL(file);
    }
</script>
@endsection
