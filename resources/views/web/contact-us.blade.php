@extends('layouts.web.app', ['page_title' => 'Contact Us'])
@section('contents')
<div class="container-fluid page-header py-5 mb-5 wow fadeIn" data-wow-delay="0.1s">
    <div class="container py-5">
        <h1 class="display-3 text-white animated slideInRight">Contact Us</h1>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb animated slideInRight mb-0">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
            </ol>
        </nav>
    </div>
</div>


<div class="container-xxl py-5">
    <div class="container">
        <div class="row g-5 justify-content-center mb-5">
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                <div class="bg-light text-center h-100 p-5">
                    <div class="btn-square bg-white rounded-circle mx-auto mb-4" style="width: 90px; height: 90px;">
                        <i class="fa fa-phone-alt fa-2x text-primary"></i>
                    </div>
                    <h4 class="mb-3">PBAX lines</h4>
                    <p class="mb-2">(+91-11) 2468 2251</p>
                    <p class="mb-4">(+91-11) 2468 2252</p>
                    <a class="btn btn-primary px-4" href="tel:+911124682251">Call Now <i
                            class="fa fa-arrow-right ms-2"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                <div class="bg-light text-center h-100 p-5">
                    <div class="btn-square bg-white rounded-circle mx-auto mb-4" style="width: 90px; height: 90px;">
                        <i class="fa fa-envelope-open fa-2x text-primary"></i>
                    </div>
                    <h4 class="mb-3">Email Address</h4>
                    <p class="mb-2">contact.ifcpar@cefipra.org</p>
                    <p class="mb-4">&nbsp;</p>
                    <a class="btn btn-primary px-4" href="mailto:contact.ifcpar@cefipra.org">Email Now <i
                            class="fa fa-arrow-right ms-2"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.5s">
                <div class="bg-light text-center h-100 p-5">
                    <div class="btn-square bg-white rounded-circle mx-auto mb-4" style="width: 90px; height: 90px;">
                        <i class="fa fa-map-marker-alt fa-2x text-primary"></i>
                    </div>
                    <h4 class="mb-3">Office Address</h4>
                    <p class="mb-2">Indo-French Centre for the Promotion of Advanced Research (IFCPAR/CEFIPRA)</p>
                    <p class="mb-2">5B, Ground Floor, India Habitat Centre, Lodhi Road</p>
                    <p class="mb-4">New Delhi - 110 003 (Entry from Gate No.2), India</p>
                    <a class="btn btn-primary px-4" href="https://goo.gl/maps/auzCuzo2wL5omBtaA"
                        target="blank">Direction <i class="fa fa-arrow-right ms-2"></i></a>
                </div>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-12 wow fadeInUp" data-wow-delay="0.1s">
                    <iframe style="min-height: 450px; border:0; width: 100%"
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3503.328569355857!2d77.22341751508164!3d28.589918282434667!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce2ee28ffffff%3A0x608d1305edd4e914!2sIndo-French%20Centre%20for%20the%20Promotion%20of%20Advanced%20Research!5e0!3m2!1sen!2sin!4v1670178340221!5m2!1sen!2sin" width="800" height="600" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade">
                    </iframe>
            </div>
        </div>
    </div>
</div>
@endsection