@extends('admin.master', ['page_title' => 'View All Researchers'])
@section('contents')

<link rel="stylesheet" href="{{ asset('../resources/plugins/admin/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('../resources/plugins/admin/datatables-responsive/css/responsive.bootstrap4.min.css') }}">

<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h4>View All Researchers</h4>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">View All Researchers</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <table id="dtable" class="table table-bordered table-striped">
                        <thead>
                            <th>#</th>
                            <th>Full Name</th>
                            <th>Basic Details</th>
                            <th>Area of Research</th>
                            <th class="text-center">Actions</th>
                        </thead>
                        <tbody>
                            @for ($i = 0; $i < count($all_applicants); $i++)
                                <tr>
                                    <td>{{ $i+1 }}</td>
                                    <td>{{ $all_applicants[$i]->full_name }}</td>
                                    <td>
                                        <span>Email: <label for="">{{ $all_applicants[$i]->email }}</label></span><br>
                                        <span>Mobile: <label for="">{{ $all_applicants[$i]->mobile_number }}</label></span><br>
                                        <span>Gender: <label for="">{{ $all_applicants[$i]->gender_name }}</label></span>
                                    </td>
                                    <td>
                                        <label for="">{{ $all_applicants[$i]->research_details->major_area_name ?? "N/A" }}</label> <br>
                                        @if (count($all_applicants[$i]->research_details->sub_area_of_interest_name ?? []) > 0)
                                            @foreach ($all_applicants[$i]->research_details->sub_area_of_interest_name as $sub_area)
                                                <span>- {{  $sub_area }}</span><br>
                                            @endforeach
                                        @endif                                       
                                    </td>
                                    <td class="text-center">
                                        <span>
                                            <a href="" class="btn btn-default btn-flat">
                                                <i class="fa fa-eye"></i> View
                                            </a>
                                        </span>
                                    </td>
                                </tr>
                            @endfor
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>

<script src="{{ asset('../resources/plugins/admin/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('../resources/plugins/admin/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('../resources/plugins/admin/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('../resources/plugins/admin/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
    $('#dtable').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
</script>
@endsection