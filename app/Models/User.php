<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    protected $appends  = [
        'full_name',
        'gender_name',
        'research_details'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'salutation', 'first_name', 'middle_name', 'last_name',
        'email', 'password', 'original_password', 'dob', 'gender', 'country_code',
        'mobile_number', 'status', 'role'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password', 'original_password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getFullNameAttribute() {
        $names = [$this->salutation, $this->first_name, $this->middle_name ?? "", $this->last_name ?? ""];
        return implode(' ', $names);
    }

    public function getGenderNameAttribute() {
        switch($this->gender) {
            case 1:
                return "Male";
            case 2:
                return "Female";
            default:
                return "Others";
        }
    }

    public function getResearchDetailsAttribute() {
        return ResearchDetails::where('user_id', $this->id)->first();
    }
}
