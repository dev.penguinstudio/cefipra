<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('research_details', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->string('research_id');
            $table->string('qualification');
            $table->string('major_area');
            $table->string('sub_area_of_interest');
            $table->string('work_country');
            $table->string('state');
            $table->string('district');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('research_details');
    }
};
