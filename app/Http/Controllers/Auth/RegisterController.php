<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\ResearchDetails;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\UserMetaData;
use App\Utility\Country;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        $country = (new Country())->getAllCountry();
        return view('auth.register')->with([
            'countries' => $country
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'salutation' => ['required', 'string', 'max:10'],
            'first_name' => ['required', 'string', 'max:50'],
            'middle_name' => ['nullable', 'string', 'max:50'],
            'last_name' => ['nullable', 'string', 'max:50'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'date_of_birth' => ['nullable', 'date'],
            'mobile_number' => ['required', 'numeric'],
            
            'research_id' => ['required', 'string'],
            'qualification' => ['required', 'string'],
            'major_area' => ['required', 'string'],
            'sub_area_of_interest' => ['required'],
            'work_country' => ['required'],
            'state' => ['required'],
            'district' => ['required'],
            'photograph' => ['nullable', 'image'],
            'signature' => ['nullable', 'image'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $new_user = User::create([
            'salutation' => $data['salutation'],
            'first_name' => $data['first_name'],
            'middle_name' => $data['middle_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'dob' => $data['date_of_birth'],
            'gender' => $data['gender'],
            'country_code' => $data['country'],
            'mobile_number' => $data['mobile_number'],
            'password' => Hash::make($data['password']),
            'original_password' => encrypt($data['password']),
        ]);

        $new_research = new ResearchDetails();
        $new_research->user_id = $new_user->id;
        $new_research->research_id = $data['research_id'];
        $new_research->qualification = $data['qualification'];
        $new_research->major_area = $data['major_area'];
        $new_research->sub_area_of_interest = implode(",", $data['sub_area_of_interest']);
        $new_research->work_country = $data['work_country'];
        $new_research->state = $data['state'];
        $new_research->district = $data['district'];
        $new_research->save();

        $current_user = new UserMetaData();
        $current_user->user_id = $new_user->id;
        
        if(!is_null($data['photograph'])) {
            $photograph_path = $data['photograph']->store('public/images/photograph');
            $current_user->photo = $photograph_path;
        }

        if(!is_null($data['signature'])) {
            $photograph_path = $data['signature']->store('public/images/signature');
            $current_user->sign = $photograph_path;
        }

        $current_user->save();
        return $new_user;
    }
}
