<?php

namespace App\Utility;

class GetWorkDetails {

    public function getStates($country_code) {
        if(is_null($country_code)) {
            return [];
        }

        $response = file_get_contents('http://www.cefipraonline.in/CEFIPRA/scientistprofileAction.do?hmode=getstate&countryType=' . $country_code);
        $phpResponse = (new XMLToPHP($response))->getPHPResponse();
        return $phpResponse;
    }

    public function getCities($state_id) {
        if(is_null($state_id)) {
            return [];
        }

        $response = file_get_contents('http://www.cefipraonline.in/CEFIPRA/scientistprofileAction.do?hmode=getcity&stateType=' . $state_id);
        $phpResponse = (new XMLToPHP($response))->getPHPResponse();
        return $phpResponse;
    }
}