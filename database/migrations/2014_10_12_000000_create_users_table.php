<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('salutation')->nullable();
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->longText('original_password');
            $table->date('dob')->nullable();
            $table->tinyInteger('gender')->default(1); // 1: Male 2: Female 3: Others
            $table->string('country_code')->default("IN");
            $table->string('mobile_number')->nullable();
            $table->tinyInteger('status')->default(1); // 1: Active 2: Blocked
            $table->tinyInteger('role')->default(3); // 1: Admin 2: Internal User 3: User
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
