<?php

namespace App\Models;

use App\Utility\GetSubArea;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResearchDetails extends Model
{
    use HasFactory;

    protected $appends  = [
        'major_area_name',
        'sub_area_of_interest_name',
        'research_details'
    ];

    protected $fillable = [
        'user_id',
        'research_id',
        'qualification',
        'major_area',
        'sub_area_of_interest',
        'work_country',
        'state',
        'district'
    ];

    public function getMajorAreaNameAttribute() {
        return config('misc.areaOfResearch')[$this->major_area];
    }

    public function getSubAreaOfInterestNameAttribute() {
        $all_sub_area = explode(',', $this->sub_area_of_interest);
        $final_result = [];
        foreach($all_sub_area as $area) {
            $sub_area_name = (new GetSubArea($this->major_area))->getSubAreaName($area);
            array_push($final_result, $sub_area_name);
        }
        // return implode(', ', $final_result);
        return $final_result;
    }
}
