<?php

use App\Utility\GetSubArea;
use App\Utility\GetWorkDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'internal'], function() {
    Route::get('subArea/{id}', function($id) {
        $utility = new GetSubArea($id);
        return response()->json($utility->getSubArea());
    });

    Route::get('getStates/{id}', function($id) {
        $utility = new GetWorkDetails();
        return response()->json($utility->getStates($id));
    })->name('getStates');

    Route::get('getCities/{id}', function($id) {
        $utility = new GetWorkDetails();
        return response()->json($utility->getCities($id));
    })->name('getStates');
});