<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LanguageController extends Controller
{
    //
    public function index(Request $request) {
        dd($request->all());
    }

    public function languageChange(Request $request) {
        // dd($request->all(), app()->getLocale());
        $new_lang_code = $request->lang_code;
        app()->setLocale($new_lang_code);
        session()->put('locale', $new_lang_code);
        return redirect()->back();
    }
}
