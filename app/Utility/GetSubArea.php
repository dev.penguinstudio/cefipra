<?php

namespace App\Utility;

class GetSubArea {

    private $area_id;
    public function __construct($area_id)
    {
        $this->area_id = $area_id;
    }

    public function getSubArea() {
        if(is_null($this->area_id)) {
            return [];
        }

        $response = file_get_contents('http://www.cefipraonline.in/CEFIPRA/registrationAction.do?hmode=getSubArea&majArea=' . $this->area_id);
        $phpResponse = (new XMLToPHP($response))->getPHPResponse();
        return $phpResponse;
    }

    public function getSubAreaName($sub_area_id) {
        $all_sub_area = $this->getSubArea();
        $sub_area_name = $all_sub_area[$sub_area_id];
        return $sub_area_name;
    }
}