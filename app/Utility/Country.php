<?php

namespace App\Utility;

class Country {

    public function getAllCountry() {
        $country_file = asset('../resources/json/country.json');
        $all_country = json_decode(file_get_contents($country_file));
        return $all_country;
    }
}