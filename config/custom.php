<?php

return [
    'app_name' => 'IFCPAR',
    'app_name_fr' => 'CEFIPRA',

    'subtitle' => 'Indo-French Centre for the Promotion of Advanced Research (IFCPAR)',
    'subtitle_fr' => 'Centre Franco-Indien pour la Promotion de la Recherche Avancée',

    'whatsapp_number' => '917292032035'
];